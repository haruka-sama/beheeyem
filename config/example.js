/*
 * Example file for Beheeyem's config
 * This file is never used by the bot iself, but can be
 * used as a config reference
 */

let config = {
  client: {
    token: ''
  }
};

module.exports = config;