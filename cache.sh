echo "Caching Abilities"
curl -s "https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/abilities.js" -o data/abilities.js
echo "Caching Items"
curl -s "https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/items.js" -o data/items.js
echo "Caching Moves"
curl -s "https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/moves.js" -o data/moves.js
echo "Caching Pokédex"
curl -s "https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/pokedex.js" -o data/pokedex.js
echo "Caching Smogon Formats"
curl -s "https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/formats-data.js" -o data/formats.js
echo "Caching Types"
curl -s "https://cdn.jsdelivr.net/gh/jalyna/oakdex-pokedex/data/type.json" -o data/types.json
echo "Done"
