/*
 * formats - This file manages the Smogon db for use with the dex command.
 */

// Require modules
const fs = require('fs');
const path = require('path');
const rp = require('request-promise');
const logger = require('../core/logger');
const reload = require('require-reload')(require);

// URL to fetch data from
const uri =
  'https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/formats-data.js';

// Declare the initial db
let formats = {};
try {
  formats = require('../../data/formats');
} catch (err) {
  formats = {};
}

// Function to fetch the Format DB and download it to the data folder
const cacheFile = async () => {
  const data = await rp({ uri, method: 'GET' });
  fs.writeFileSync(path.join(__dirname, '../../data/formats.js'), data);
  try {
    formats = reload('../../data/formats');
    logger.info('Cached Smogon Format DB!');
  } catch (e) {
    cacheFile();
  }
};

// Cache the data on start
setTimeout(cacheFile, 60000)

// Cache the data every 15 minutes (900 000 ms)
setInterval(cacheFile, 900000);

module.exports = formats.BattleFormatsData;
