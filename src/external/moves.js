/*
 * dex - This file manages the Move db for use with the move command.
 */

// Require modules
const fs = require('fs');
const path = require('path');
const rp = require('request-promise');
const logger = require('../core/logger');
const reload = require('require-reload')(require);

// URL to fetch data from
const uri =
  'https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/moves.js';

// Declare the initial db
let moves = {};
try {
  moves = require('../../data/moves');
} catch (err) {
  moves = {};
}

// Function to fetch the Move DB and download it to the data folder
const cacheFile = async () => {
  const data = await rp({ uri, method: 'GET' });
  fs.writeFileSync(path.join(__dirname, '../../data/moves.js'), data);
  try {
    moves = reload('../../data/moves');
    logger.info('Cached Move DB!');
  } catch (e) {
    cacheFile();
  }
};

// Cache the data on start
setTimeout(cacheFile, 60000)

// Cache the data every 15 minutes (900 000 ms)
setInterval(cacheFile, 900000);

module.exports = moves.BattleMovedex;
