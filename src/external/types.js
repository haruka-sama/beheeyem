/*
 * dex - This file manages the Type db for use with the type command.
 */

// Require modules
const fs = require('fs');
const path = require('path');
const rp = require('request-promise');
const logger = require('../core/logger');
const reload = require('require-reload')(require);

// URL to fetch data from
const uri =
  'https://cdn.jsdelivr.net/gh/jalyna/oakdex-pokedex/data/type.json';

// Declare the initial db
let types = {};
try {
  types = require('../../data/types.json');
} catch (err) {
  types = {};
}

// Function to fetch the Pokédex file and download it to the data folder
const cacheFile = async () => {
  const data = await rp({ uri, method: 'GET' });
  fs.writeFileSync(path.join(__dirname, '../../data/types.json'), data);
  try {
    types = reload('../../data/types.json');
    logger.info('Cached Type DB!');
  } catch (e) {
    cacheFile();
  }
};

// Cache the data on start
setTimeout(cacheFile, 60000)

// Cache the data every 15 minutes (900 000 ms)
setInterval(cacheFile, 900000);

module.exports = types;
