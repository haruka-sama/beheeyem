/*
 * dex - This file manages the Pokédex db for use with the dex command.
 */

// Require modules
const request = require('request');
const logger = require('../core/logger');

// URL to fetch data from
const fetchBase =
  'https://cdn.jsdelivr.net/gh/jalyna/oakdex-pokedex/data/pokemon';

const fetch = pokemon => {
  return new Promise((resolve, reject) => {
    const fetchURL = `${fetchBase}/${pokemon}.json`;
    request.get({ url: fetchURL, json: true }, (e, r, data) => {
      if (!e) {
        logger.info(`Fetched dex entry for ${pokemon}`);
        resolve(data.pokedex_entries);
      } else {
        logger.error(`Error fetching dex entry for ${pokemon}: ${e}`);
        resolve.reject(e);
      }
    });
  });
};

module.exports = fetch;
