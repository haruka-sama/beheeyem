/*
 * dex - This file manages the Pokédex db for use with the dex command.
 */

// Require modules
const path = require('path');
const fs = require('fs');
const rp = require('request-promise');
const logger = require('../core/logger');
const reload = require('require-reload')(require);

// URL to fetch data from
const uri =
  'https://raw.githubusercontent.com/smogon/pokemon-showdown/master/data/pokedex.js';

// Declare the initial Pokédex
let pokedex = {};
try {
  pokedex = require('../../data/pokedex');
  for (const i in pokedex.BattlePokedex) {
    pokedex.BattlePokedex[i].species = pokedex.BattlePokedex[i].species.replace(/-/g, ' ');
  }
} catch (err) {
  pokedex = {};
}

// Function to fetch the Pokédex file and download it to the data folder
const cacheFile = async () => {
  const data = await rp({ uri, method: 'GET' });
  fs.writeFileSync(path.join(__dirname, '../../data/pokedex.js'), data);
  try {
    pokedex = reload('../../data/pokedex');
    logger.info('Cached Pokédex!');
  } catch (e) {
    cacheFile();
  }
}

// Cache the data on start
setTimeout(cacheFile, 60000);

// Cache the data every 15 minutes (900 000 ms)
setInterval(cacheFile, 900000);

module.exports = pokedex.BattlePokedex;
