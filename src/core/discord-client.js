/*
 * This file contains Beheeyem's Discord client
 */

// Require modules and create the client
const logger = require('./logger');
const Discord = require('discord.js');
const msgHandler = require('./message-handler');
const config = require('../../config');
const commands = require('../commands');
const client = new Discord.Client({ disabledEvents: ['TYPING_START', 'PRESENCE_UPDATE'] });

// When the client logs in, log
client.on('ready', () => {
  logger.info(`Client logged in as ${client.user.tag} on shard ${client.shard.id}`);
  msgHandler.init(client);
  client.shard.send({ type: 'guildCount', count: client.guilds.size });
});

commands.fetch();
client.login(config.client.token);

module.exports = client;
