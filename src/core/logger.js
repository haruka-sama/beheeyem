/*
 * This file contains the main logger for Beheeyem
 */

const winston = require('winston');

const logger = winston.createLogger({
  levels: winston.config.syslog.levels,
  format: winston.format.printf(info => `${new Date()} - ${info.message}`),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({
      filename: './logs/error.log',
      level: 'error'
    }),
    new winston.transports.File({
      filename: './logs/combined.log',
      level: 'debug'
    }),
    new winston.transports.Console({
      timestamp: true,
      level: 'debug',
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      )
    })
  ]
});

module.exports = logger;
