const { existsSync, readFileSync, writeFileSync } = require('fs');
const { join } = require('path');

let settings = existsSync(join(__dirname, '../../config/settings.json'))
  ? JSON.parse(readFileSync(join(__dirname, '../../config/settings.json')))
  : {};

const props = {
  customPrefix: {
    description: 'Beheeyem\'s prefix for the guild',
    scope: 'guild'
  },
  autoSpriteLimit: {
    description: 'Sets the maximum number of auto-sprites to render for a channel',
    scope: 'channel'
  },
  disableAutoSprite: {
    description: 'Disables Beheeyem\'s auto-sprite feature for a channel',
    scope: 'channel'
  }
}

const syncSettings = () => {
  // Reload the settings object to avoid shards deleting settings
  const load = JSON.parse(readFileSync(join(__dirname, '../../config/settings.json')));
  settings = Object.assign(load, settings);
}

// Change a setting
const set = (id, prop, value) => {
  if (!props[prop]) return undefined;

  syncSettings();

  // Tidying
  if (!settings[id]) settings[id] = {};
  if (value === 'true') value = true;
  if (value === 'false') value = false;
  if (!isNaN(value)) value = Number(value);

  // Save the value
  settings[id][prop] = value;
  writeFileSync(join(__dirname, '../../config/settings.json'), JSON.stringify(settings, null, 2));
  return value;
}

// Return a setting
const get = (id, prop) => {
  if (!props[prop]) return undefined;
  if (!settings[id]) settings[id] = {};
  return settings[id][prop];
}

// Set an alias
const setAlias = (id, name, value) => {
  syncSettings();

  // Set the alias
  if (!settings[id]) settings[id] = {};
  if (!settings[id].aliases) settings[id].aliases = {};
  settings[id].aliases[name] = value;
  writeFileSync(join(__dirname, '../../config/settings.json'), JSON.stringify(settings, null, 2));
  return value;
}

// Return an alias
const getAlias = (id, name) => {
  if (!settings[id]) return undefined;
  if (!settings[id].aliases) return undefined;
  if (typeof name !== 'undefined') {
    return settings[id].aliases[name];
  } else {
    return settings[id].aliases;
  }
}

const removeAlias = (id, name) => {
  syncSettings();
  const removed = getAlias(id, name);
  if (removed) {
    delete settings[id].aliases[name];
    writeFileSync(join(__dirname, '../../config/settings.json'), JSON.stringify(settings, null, 2));
  }
  return removed;
}

module.exports = { set, get, props, setAlias, getAlias, removeAlias };
