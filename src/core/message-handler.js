/*
 * This file is the initial message handler for Beheeyem
 */

const logger = require('./logger');
const commandHandler = require('./command-handler');
const autoSprite = require('./auto-sprite');
const config = require('../../config');
const settings = require('./settings');
const { join } = require('path');

const init = (client) => {
  // Main event to handle messages
  client.on('message', async msg => {
    // Ignore messages from bots
    if (msg.author.bot) return;

    // Check if message starts with the command prefix
    if (msg.content.startsWith(config.bot.prefix) ||
      (msg.content.startsWith(settings.get(msg.guild.id, 'customPrefix')))) {
      // Send the message to the command handler if so
      commandHandler.handle({ msg, client });
    } else if ((msg.content.includes('*') || msg.content.includes('_')) &&
      !settings.get(msg.channel.id, 'disableAutoSprite')) {
      // Auto spriting for italicized text
      const split = [...msg.content.split('*'), ...msg.content.split('_')];
      var sent = 0;
      for (let i = 0; i < split.length; i++) {
        if (split[i] === '') continue;
        const query = settings.getAlias(msg.guild.id, split[i])
          ? settings.getAlias(msg.guild.id, split[i])
          : split[i];
        if (query === 'dab') {
          msg.channel.send({ files: [join(__dirname, '../../data/kadabra.png')] });
          sent++;
        } else {
          const sprite = await autoSprite(query);
          if (sprite) {
            msg.channel.send({ files: [sprite] });
            logger.info(`Sent auto-sprite for ${query} to guild ${msg.guild.name}`);
            sent++;
          }
        }
        if (sent == (settings.get(msg.channel.id, 'autoSpriteLimit') || 3)) break;
      }
    }
  });
}

module.exports.init = init;
