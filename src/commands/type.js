/*
 * move
 *
 * This command returns information on a move.
 *
 */

// Require modules
const logger = require('../core/logger');
const Discord = require('discord.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const typeCommand = new Command();

// Set the display attributes for the command
typeCommand.description = 'Show type effectiveness for one or more types';
typeCommand.usage = 'type <name> [name] ...';

const types = require('../external/types');

const sortDamages = inObj => {
  const out = {
    strong: [],
    normal: [],
    weak: [],
    nullified: []
  };
  for (let i = 0; i < Object.keys(inObj).length; i++) {
    if (inObj[Object.keys(inObj)[i]] > 1) {
      out.strong.push(
        `${Object.keys(inObj)[i]} (x${inObj[Object.keys(inObj)[i]]})`
      );
    } else if (inObj[Object.keys(inObj)[i]] === 1) {
      out.normal.push(Object.keys(inObj)[i]);
    } else if (inObj[Object.keys(inObj)[i]] === 0) {
      out.nullified.push(Object.keys(inObj)[i]);
    } else {
      out.weak.push(
        `${Object.keys(inObj)[i]} (x${inObj[Object.keys(inObj)[i]]})`
      );
    }
  }
  return out;
};

// Set the command's action
typeCommand.execute = async ({ msg, args }) => {
  const baseEmbed = new Discord.RichEmbed();
  const baseDmg = {};
  const sorted = {};

  // Search for the ability
  const tLow = args.split(' ').map(a => a.toLowerCase());
  const tKey = tLow.map(l => l.charAt(0).toUpperCase() + l.slice(1));
  const type = [];
  for (let i = 0; i < tKey.length; i++) {
    if (types[tKey[i]]) {
      type.push(types[tKey[i]]);
    } else {
      tKey.splice(i, 1);
      i--;
    }
  }

  // If the type is not found, return an error
  if (type.length < 1) {
    msg.channel.send('⚠ Type not found. Check your spelling and try again.');
  }

  // Initialize embed
  baseEmbed.setTitle(tKey.join(', '));

  // Offensive calculations
  baseDmg.offense = {};
  for (let i = 0; i < type.length; i++) {
    for (let j = 0; j < Object.keys(type[i].effectivness).length; j++) {
      const cType = Object.keys(type[i].effectivness)[j];
      if (typeof baseDmg.offense[cType] === 'undefined') {
        baseDmg.offense[cType] = type[i].effectivness[cType];
      } else {
        baseDmg.offense[cType] *= type[i].effectivness[cType];
      }
    }
  }
  sorted.offense = sortDamages(baseDmg.offense);
  let dmgStrings = [];
  if (sorted.offense.strong.length > 0) {
    dmgStrings.push(
      `Super effective against:\n${sorted.offense.strong.join(', ')}`
    );
  }
  if (sorted.offense.normal.length > 0) {
    dmgStrings.push(
      `Deals normal damage against:\n${sorted.offense.normal.join(', ')}`
    );
  }
  if (sorted.offense.weak.length > 0) {
    dmgStrings.push(
      `Not very effective against:\n${sorted.offense.weak.join(', ')}`
    );
  }
  if (sorted.offense.nullified.length > 0) {
    dmgStrings.push(`Does not affect:\n${sorted.offense.nullified.join(', ')}`);
  }

  baseEmbed.fields.push({
    name: 'Offense',
    value: dmgStrings.join('\n\n') + '\n\u200b'
  });

  // Defensive calculations
  baseDmg.defense = {};
  for (let i = 0; i < type.length; i++) {
    for (let j = 0; j < Object.keys(types).length; j++) {
      const cType = Object.keys(types)[j];
      if (typeof baseDmg.defense[cType] === 'undefined') {
        baseDmg.defense[cType] = types[cType].effectivness[tKey[i]];
      } else {
        baseDmg.defense[cType] *= types[cType].effectivness[tKey[i]];
      }
    }
  }
  sorted.defense = sortDamages(baseDmg.defense);
  dmgStrings = [];
  if (sorted.defense.strong.length > 0) {
    dmgStrings.push(`Weak to:\n${sorted.defense.strong.join(', ')}`);
  }
  if (sorted.defense.normal.length > 0) {
    dmgStrings.push(
      `Takes normal damage from:\n${sorted.defense.normal.join(', ')}`
    );
  }
  if (sorted.defense.weak.length > 0) {
    dmgStrings.push(`Resists:\n${sorted.defense.weak.join(', ')}`);
  }
  if (sorted.defense.nullified.length > 0) {
    dmgStrings.push(
      `Takes no damage from:\n${sorted.defense.nullified.join(', ')}`
    );
  }

  baseEmbed.fields.push({
    name: 'Defense',
    value: dmgStrings.join('\n\n') + '\n\u200b'
  });

  baseEmbed.fields.push({
    name: 'External Resources',
    value: externalString()
  })

  msg.channel.send({ embed: baseEmbed });
  logger.info(`Sent typing info for ${args} to guild ${msg.guild.name}`);
};

// Function to generate the "External Resources" part of the embed
const externalString = () => {
  // Empty array to store link strings
  const externals = [];

  // Generate links for each site
  externals.push(
    `[Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Type#Type_chart)`
  );
  externals.push(`[Smogon](https://www.smogon.com/dex/sm/types/)`);
  externals.push(`[PokémonDB](https://pokemondb.net/type)`);

  // Return concatenated list
  return externals.join(' • ');
};

module.exports = typeCommand;
