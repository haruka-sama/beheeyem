/*
 * slowpoke
 *
 * This command sends a Slowpoke after a varying amount of time.
 *
 */

// Require modules
const { join } = require('path');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const slowpoke = new Command();

slowpoke.hidden = true;

// Set the command's action
slowpoke.execute = ({ msg }) => {
  setTimeout(() => {
    msg.channel.send({ files: [{ attachment: join(__dirname, '../../data/slowpoke.png') }] });
  }, Math.floor(Math.random() * 240000 + 60000));
};

module.exports = slowpoke;
