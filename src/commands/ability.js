/*
 * dex
 *
 * This command returns information on an ability.
 *
 */

// Require modules
const logger = require('../core/logger');
const Discord = require('discord.js');
const Fuse = require('fuse.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const abilityCommand = new Command();

// Get external data
const abilities = require('../external/abilities');

// Set the display attributes for the command
abilityCommand.description = 'Fetch info on an ability';
abilityCommand.usage = 'ability <name>';

// Rating code descriptions
const ratings = {
  '-2': {
    desc: 'Extremely detrimental',
    color: 'RED'
  },
  '-1': {
    desc: 'Detrimental',
    color: 'RED'
  },
  0: {
    desc: 'Useless',
    color: ''
  },
  1: {
    desc: 'Ineffective',
    color: ''
  },
  2: {
    desc: 'Situationally useful',
    color: 'BLUE'
  },
  3: {
    desc: 'Useful',
    color: 'BLUE'
  },
  4: {
    desc: 'Very useful',
    color: 'GREEN'
  },
  5: {
    desc: 'Essential',
    color: 'GOLD'
  }
};

var searchOptions = {
  shouldSort: true,
  tokenize: true,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    'name',
    'id',
    'desc'
  ]
};
const fuse = new Fuse(Object.values(abilities), searchOptions);

// Set the command's action
abilityCommand.execute = ({ msg, args }) => {
  const baseEmbed = new Discord.RichEmbed();

  // Search for the ability
  const results = fuse.search(args);

  // If the ability is not found, return an error
  if (results.length === 0) {
    msg.channel.send('⚠ Ability not found. Check your spelling and try again.');
    logger.error(`Unable to find ability ${args} for guild ${msg.guild.name}`);
    return;
  }

  const ability = results[0];

  // Initialize embed
  baseEmbed.setTitle(ability.name);

  baseEmbed.setDescription('\u200b\n' + (ability.desc || ability.shortDesc));

  baseEmbed.fields.push({
    name: 'Smogon Rating',
    value: `${ratings[Math.floor(ability.rating)].desc} (${ability.rating})`
  });

  baseEmbed.fields.push({
    name: 'External Resources',
    value: externalString(ability.name)
  });

  baseEmbed.setColor(ratings[Math.floor(ability.rating)].color);

  msg.channel.send({ embed: baseEmbed });
  logger.info(`Sent ability ${ability.name} to guild ${msg.guild.name}`);
};

// Function to generate the "External Resources" part of the embed
const externalString = name => {
  // Empty array to store link strings
  const externals = [];

  // Generate links for each site
  externals.push(
    `[Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/${name.replace(
      / /g,
      '_'
    )}_(Ability\\))`
  );
  externals.push(
    `[Smogon](https://www.smogon.com/dex/sm/abilities/${name
      .toLowerCase()
      .replace(/ /g, '_')})`
  );
  externals.push(
    `[PokémonDB](https://pokemondb.net/ability/${name
      .toLowerCase()
      .replace(/ /g, '-')})`
  );

  // Return concatenated list
  return externals.join(' • ');
};

module.exports = abilityCommand;
