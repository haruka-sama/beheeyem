/*
 * convert
 *
 * This command allows conversion between two units.
 *
 */

// Require modules
const logger = require('../core/logger');
const { unit } = require('mathjs');
const { RichEmbed } = require('discord.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const convert = new Command();

// Set the display attributes for the command
convert.description = 'Convert between two units.';
convert.usage = 'convert <value> to <target unit>';

// Set the command's action
convert.execute = ({ msg, args }) => {
  const split = args.split(' ');
  const target = split[split.length - 1];
  split.splice(split.length - 1, 1);
  if (split[split.length - 1] === 'to') split.splice(split.length - 1, 1);

  const source = split.join(' ');
  let result;
  const embed = new RichEmbed();
  embed.setTitle('Unit Conversion');
  try {
    result = unit(source).toNumber(target).toPrecision(4);
    embed.addField('Input', source, true)
      .addField('Output', `${result} ${target}`, true);
    logger.info(`Converted ${source} to ${target} for guild ${msg.guild.name}`)
  } catch (err) {
    logger.error(`Error converting ${source} to ${target} for guild ${msg.guild.name}`);
    embed.setColor('RED')
      .setDescription('There was an error parsing your query. Check the [list of accepted units](https://mathjs.org/docs/datatypes/units.html#reference) and try again.');
  }

  msg.channel.send({ embed });
};

module.exports = convert;
