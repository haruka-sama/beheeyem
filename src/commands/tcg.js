/*
 * tcg
 *
 * This command fetches/searches Pokemon TCG cards.
 *
 */

// Require modules
const logger = require('../core/logger');
const { card } = require('pokemontcgsdk');
const { RichEmbed } = require('discord.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const tcgCommand = new Command();

// Set the display attributes for the command
tcgCommand.description = 'Show and search for TCG cards';
tcgCommand.usage = 'tcg search <name>\ntcg search <set code> <name>';

// Set the command's action
tcgCommand.execute = async ({ msg, args }) => {
  if (args.split(' ').length < 1) return;

  const split = args.split(' ');
  const subCmd = split[0];

  if (subCmd === 'search') {
    let name = split.map(c => c.toLowerCase());
    let subtype;
    name.splice(0, 1);
    let subIndex = name.indexOf('mega');
    if (subIndex > -1) {
      subtype = 'mega';
      name.splice(subIndex, 1);
    }
    subIndex = name.indexOf('ex');
    if (subIndex > -1) {
      subtype = 'EX';
      name.splice(subIndex, 1);
    }
    name = name.join(' ');
    const resultMsg = await msg.channel.send('<a:loading:662040727219601441> Fetching data...');
    const cards = await card.where({ name, subtype });
    const embed = new RichEmbed();
    embed.setTitle(`Results for "${name}" ${subtype ? `(${subtype})` : ''}`);
    if (cards.length > 0) {
      embed.setThumbnail(cards[0].imageUrl);
      for (let i = 0; i < cards.length && i < 9; i++) {
        embed.addField(cards[i].supertype, `${cards[i].name}\n*${cards[i].set} (${cards[i].setCode})*`, true);
      }
      embed.setDescription(`[Click me](https://pokemontcg.io/cards?name=${encodeURIComponent(name)}) for a full list of cards.`);
      logger.info(`Sending TCG search results for ${name} for guild ${msg.guild.name}`);
    } else {
      embed.setDescription('No matches found.');
    }
    resultMsg.edit({ embed });
  } else if (subCmd === 'show') {
    if (split.length < 3) return;

    let name = split.map(c => c.toLowerCase());
    name.splice(0, 1);
    const setCode = name.splice(0, 1)[0];
    let subtype;
    let subIndex = name.indexOf('mega');
    if (subIndex > -1) {
      subtype = 'mega';
      name.splice(subIndex, 1);
    }
    subIndex = name.indexOf('ex');
    if (subIndex > -1) {
      subtype = 'EX';
      name.splice(subIndex, 1);
    }
    name = name.join(' ');

    const resultMsg = await msg.channel.send('<a:loading:662040727219601441> Fetching data...');
    const cards = await card.where({ name, subtype, setCode });
    if (cards.length > 0) {
      const tCard = cards[0];
      const embed = new RichEmbed();
      embed.setTitle(tCard.name)
        .setImage(tCard.imageUrlHiRes || tCard.imageUrl)
        .addField('Set', tCard.set, true)
        .addField('Artist', tCard.artist, true)
        .setDescription(`[More info](https://pokemontcg.io/cards/${tCard.id})`);
      resultMsg.edit({ embed });
      logger.info(`Sent TCG card ${tCard.name} to guild ${msg.guild.name}`);
    } else {
      resultMsg.edit('Card not found. Remember to use the formatting `<set code> <name>`');
      logger.error(`Unablet to find TCG card ${name} for guild ${msg.guild.name}`);
    }
  }
};

module.exports = tcgCommand;
