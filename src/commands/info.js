/*
 * info
 *
 * This command shows info on Beheeyem.
 *
 */

// Require modules
const logger = require('../core/logger');
const { RichEmbed } = require('discord.js');
const { duration } = require('moment');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const info = new Command();

// Set the display attributes for the command
info.description = 'Show info on the bot';
info.usage = 'info';

// Set the command's action
info.execute = async ({ msg, client }) => {
  const embed = new RichEmbed();
  embed.setTitle('Beheeyem')
    .setColor('#C2926E')
    .setThumbnail(client.user.displayAvatarURL);

  const promises = [
    client.shard.fetchClientValues('guilds.size'),
    client.shard.broadcastEval('this.guilds.reduce((prev, guild) => prev + guild.memberCount, 0)')
  ];
  const data = await Promise.all(promises);

  embed.addField('Guilds', data[0], true);
  embed.addField('Users', data[1], true);

  const uptime = duration(client.uptime, 'milliseconds').humanize()
  embed.addField('Uptime', uptime.replace(/^\w/, c => c.toUpperCase()));

  msg.channel.send({ embed });
  logger.info(`Sent an info page to guild ${msg.guild.name}`);
};

module.exports = info;
