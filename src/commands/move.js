/*
 * move
 *
 * This command returns information on a move.
 *
 */

// Require modules
const logger = require('../core/logger');
const Discord = require('discord.js');
const rp = require('request-promise');
const Fuse = require('fuse.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const moveCommand = new Command();

// Get external data
const moves = require('../external/moves');

// Set the display attributes for the command
moveCommand.description = 'Show info on a move';
moveCommand.usage = 'move <name>';

const baseURL =
  'https://cdn.jsdelivr.net/gh/jalyna/oakdex-pokedex/data/move';

const terminology = {
  targets: {
    normal: 'One Enemy',
    adjacentAlly: 'One Ally',
    self: 'Self',
    allAdjacent: 'All Pokémon',
    allySide: 'Ally Team',
    allyTeam: 'Ally Team',
    foeSide: 'Opponent Team'
  },
  categories: {
    Physical: '603419123891961866',
    Special: '603419125036875776',
    Status: '603419123837566976'
  }
};

var searchOptions = {
  shouldSort: true,
  tokenize: true,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    'name',
    'num',
    'desc',
    'type',
    'category'
  ]
};
const fuse = new Fuse(Object.values(moves), searchOptions);

// Set the command's action
moveCommand.execute = async ({ msg, args }) => {
  const baseEmbed = new Discord.RichEmbed();

  // Search for the ability
  const results = fuse.search(args);

  // If the move is not found, return an error
  if (results.length === 0) {
    msg.channel.send('⚠ Move not found. Check your spelling and try again.');
    return;
  }

  const move = results[0];

  const cleanName = move.name.toLowerCase().replace(/[-, ]/g, '_');
  let flavourText;
  try {
    flavourText = await rp.get({
      url: `${baseURL}/${cleanName}.json`,
      json: true
    });
    flavourText = flavourText.pokedex_entries;
    flavourText = Object.keys(flavourText).reduce((r, e) => {
      if (flavourText[e].en !== 'Unknown') r[e] = flavourText[e];
      return r;
    }, {});

    baseEmbed.setDescription(
      '\u200b\n*' +
      Object.values(flavourText)[Object.values(flavourText).length - 1].en +
      '*\n\n' +
      (move.desc || move.shortDesc)
    );
  } catch (err) { }

  // Initialize embed
  baseEmbed.setTitle(
    `${msg.client.emojis.get(terminology.categories[move.category])} ${
      move.name
    } ${move.isViable ? msg.client.emojis.get('603413577621176333') : ''}`
  )
  baseEmbed.fields.push({
    name: 'Type',
    value: move.type,
    inline: true
  });

  baseEmbed.fields.push({
    name: 'PP',
    value: move.pp,
    inline: true
  });

  if (move.basePower > 0) {
    baseEmbed.fields.push({
      name: 'Base Power',
      value: move.basePower,
      inline: true
    });
  }

  baseEmbed.fields.push({
    name: 'Accuracy',
    value: move.accuracy === true ? '—' : move.accuracy + '%',
    inline: true
  });

  baseEmbed.fields.push({
    name: 'Target',
    value: terminology.targets[move.target] || move.target,
    inline: true
  });

  baseEmbed.fields.push({
    name: 'Priority',
    value: move.priority,
    inline: true
  });

  baseEmbed.fields.push({
    name: 'External Resources',
    value: externalString(move.name)
  });

  msg.channel.send({ embed: baseEmbed });
  logger.info(`Sent move ${move.name} to guild ${msg.guild.name}`);
};

// Function to generate the "External Resources" part of the embed
const externalString = name => {
  // Empty array to store link strings
  const externals = [];

  // Generate links for each site
  externals.push(
    `[Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/${name.replace(
      / /g,
      '_'
    )}_(move\\))`
  );
  externals.push(
    `[Smogon](https://www.smogon.com/dex/ss/moves/${name
      .toLowerCase()
      .replace(/ /g, '_')})`
  );
  externals.push(
    `[PokémonDB](https://pokemondb.net/move/${name
      .toLowerCase()
      .replace(/ /g, '-')})`
  );
  externals.push(
    `[Serebii](https://www.serebii.net/attackdex-ss/${name
      .toLowerCase()
      .replace(/ /g, '')}.shtml)`
  );

  // Return concatenated list
  return externals.join(' • ');
};

module.exports = moveCommand;
