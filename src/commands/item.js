/*
 * item
 *
 * This command returns information on an item
 *
 */

// Require the logger
const logger = require('../core/logger');
const Discord = require('discord.js');
const Fuse = require('fuse.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const itemCommand = new Command();

// Get external data
const items = require('../external/items');

// Set the display attributes for the command
itemCommand.description = 'Shows info on an item';
itemCommand.usage = 'item <name>';

var searchOptions = {
  shouldSort: true,
  tokenize: true,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    'name',
    'num',
    'desc'
  ]
};
const fuse = new Fuse(Object.values(items), searchOptions);

// Set the command's action
itemCommand.execute = ({ msg, args }) => {
  const baseEmbed = new Discord.RichEmbed();

  // Search for the ability
  const results = fuse.search(args);

  // If the ability is not found, return an error
  if (results.length === 0) {
    msg.channel.send('⚠ Item not found. Check your spelling and try again.');
    return;
  }

  const item = results[0];

  // Initialize embed
  baseEmbed.setTitle(item.name);

  baseEmbed.setDescription('\u200b\n' + (item.desc || item.shortDesc));

  baseEmbed.fields.push({
    name: 'Generation Introduced',
    value: item.gen,
    inline: true
  });

  baseEmbed.fields.push({
    name: 'External Resources',
    value: externalString(item.name)
  });

  baseEmbed.setFooter(`#${item.num}`);

  msg.channel.send({
    embed: baseEmbed
  });
  logger.info(`Sent item ${item.name} to guild ${msg.guild.name}`);
};

// Function to generate the "External Resources" part of the embed
const externalString = name => {
  // Empty array to store link strings
  const externals = [];

  // Generate links for each site
  externals.push(
    `[Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/${name.replace(
      / /g,
      '_'
    )}_(Item\\))`
  );
  externals.push(
    `[Smogon](https://www.smogon.com/dex/ss/items/${name
      .toLowerCase()
      .replace(/ /g, '_')})`
  );
  externals.push(
    `[PokémonDB](https://pokemondb.net/item/${name
      .toLowerCase()
      .replace(/ /g, '-')})`
  );
  externals.push(
    `[Serebii](https://www.serebii.net/itemdex/${name
      .toLowerCase()
      .replace(/ /g, '')}.shtml)`
  );

  // Return concatenated list
  return externals.join(' • ');
};

module.exports = itemCommand;
