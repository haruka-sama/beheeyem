/*
* set
*
* This command is used by moderators to modify Beheeyem's settings for a guild.
*
*/

// Require modules
const logger = require('../core/logger');
const settings = require('../core/settings');
const config = require('../../config');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const set = new Command();

// Set the display attributes for the command
set.description = 'Change a setting';
set.usage = 'set <property> <value>';

// Set the command's action
set.execute = ({ msg, args }) => {
  // Check permissions
  if (!msg.member.hasPermission('MANAGE_GUILD') && config.bot.ownerIDs.indexOf(msg.author.id) < 0) {
    logger.error(`Denied ${msg.author.tag} from modifying settings for guild ${msg.guild.name}`);
    msg.channel.send('You don\'t have permission to use this command. Please contact a server moderator.');
    return;
  }

  // Check to see if property is valid
  const prop = args.split(' ')[0];
  if (!settings.props[prop]) {
    msg.channel.send(`Setting \`${prop}\` not found.`);
    return;
  }

  // Parse values and check setting scope
  let value = args.split(' ');
  value.splice(0, 1);
  value = value.join(' ');
  let id;
  const scope = settings.props[prop].scope;
  if (scope === 'guild') {
    id = msg.guild.id;
  } else if (scope === 'channel') {
    id = msg.channel.id;
  } else if (scope === 'user') {
    id = msg.author.id;
  }

  // Change setting and log
  settings.set(id, prop, value);
  logger.info(`Set ${prop} to ${value} for ${settings.props[prop].scope} ${id}`);
  msg.channel.send(`Successfully changed setting \`${prop}\` to \`${value}\` for ${
    scope === 'user' ? 'you' : `this ${scope}`
  }!`);
};

module.exports = set;
