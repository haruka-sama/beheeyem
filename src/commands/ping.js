/*
 * ping
 *
 * This command returns with a message detailing Beheeyem's response time.
 *
 */

// Require the logger
const logger = require('../core/logger');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const ping = new Command();

// Set the display attributes for the command
ping.description = 'Check Beheeyem\'s current response time';
ping.usage = 'ping';

// Set the command's action
ping.execute = ({ msg }) => {
  logger.info(`Sending a ping to guild ${msg.guild.nam}`);
  // Send the initial response message
  msg.channel.send('Pong!').then(msg2 => {
    // Get the difference in timestamps between the message and the response
    const ms = msg2.createdTimestamp - msg.createdTimestamp;

    // Edit the sent message to display the delay
    msg2.edit(`Pong! - ${ms}ms`);
    logger.info(`Ping was ${ms}ms`);
  });
};

module.exports = ping;
