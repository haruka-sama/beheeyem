/*
* alias
*
* This command returns with a message detailing Beheeyem's response time.
*
*/

// Require modules
const logger = require('../core/logger');
const config = require('../../config');
const settings = require('../core/settings');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const aliasCmd = new Command();

// Set the display attributes for the command
aliasCmd.description = 'Set or remove an alias.';
aliasCmd.usage = 'alias set <name> <pokemon>\nalias remove <name>';

// Set the command's action
aliasCmd.execute = ({ msg, args }) => {
  // Check permissions
  if (!msg.member.hasPermission('MANAGE_GUILD') && config.bot.ownerIDs.indexOf(msg.author.id) < 0) {
    logger.error(`Denied ${msg.author.tag} from modifying settings for guild ${msg.guild.name}`);
    msg.channel.send('You don\'t have permission to use this command. Please contact a server moderator.');
    return;
  }

  const split = args.split(' ');
  if (split.length < 2) {
    msg.channel.send('Missing arguments. See the help page for this command for proper formatting.');
    return;
  }

  const subCmd = split[0];
  const name = split[1];

  if (subCmd === 'set') {
    const value = args.substring(subCmd.length + name.length + 2);
    settings.setAlias(msg.guild.id, name, value);
    msg.channel.send(`Set alias \`${name}\` to \`${value}\` for this guild!`);
  } else if (subCmd === 'remove') {
    if (settings.removeAlias(msg.guild.id, name)) {
      msg.channel.send(`Alias \`${name}\` removed for this guild.`)
    }
  }
};

module.exports = aliasCmd;
