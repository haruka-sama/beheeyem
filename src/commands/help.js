/*
 * help
 *
 * This command provides information on available commands.
 *
 */

// Require modules
const { RichEmbed } = require('discord.js');
const logger = require('../core/logger');
const config = require('../../config');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const help = new Command();

// Set the display attributes for the command
help.description = 'List and detail available commands';
help.usage = 'help [command]';

// Set the command's action
help.execute = ({ msg, args, commands }) => {
  if (args) {
    // Show details on a specific command
    const name = args.split(' ')[0].toLowerCase();
    const cmd = commands[name];
    if (cmd) {
      const embed = new RichEmbed();
      embed.setTitle(`${config.bot.prefix}${name}`)
        .setColor('#C2926E')
        .addField('Usage', cmd.usage.split('\n').map(u => `\`${config.bot.prefix}${u}\``).join('\n'))
        .setDescription(cmd.description);
      msg.channel.send({ embed });
      logger.info(`Sent help for command ${name} to ${msg.guild.name}`);
    } else {
      msg.channel.send('Couldn\'t find that command. Check your spelling and try again.');
      logger.error(`Unable to find command ${name} for guild ${msg.guild.name}`);
    }
  } else {
    // Show a list of commands
    const embed = new RichEmbed();
    embed.setTitle('List of Commands')
      .setColor('#C2926E')
      .setDescription(`Use \`${config.bot.prefix}help <command>\` for additional info on a specific command.`);
    Object.keys(commands).forEach(i => {
      if (commands[i].hidden) return;
      embed.addField(config.bot.prefix + i,
        `${commands[i].usage.split('\n').map(u => `\`${config.bot.prefix}${u}\``).join('\n')}\n*${commands[i].description}*`,
        true);
    });
    msg.channel.send({ embed });
    logger.info(`Sent a list of commands to guild ${msg.guild.name}`);
  }
};

module.exports = help;
