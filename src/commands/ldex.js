/*
 * ldex
 *
 * This command returns a lightweight Pokédex entry.
 *
 */

// Require modules
const logger = require('../core/logger');
const Discord = require('discord.js');
const Fuse = require('fuse.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const dex = new Command();

// Get external data
const pokedex = require('../external/dex');

// Tags for stat display
const statTags = {
  hp: 'HP',
  atk: 'ATK',
  def: 'DEF',
  spa: 'SPATK',
  spd: 'SPDEF',
  spe: 'SPD'
};

// Colours for Pokédex colour entry
const colours = {
  Red: 'RED',
  Blue: 'BLUE',
  Yellow: 'GOLD',
  Green: 'GREEN',
  Black: 'DARKER_GREY',
  Brown: 'DARK_GOLD',
  Purple: 'DARK_PURPLE',
  Gray: 'GREY',
  White: 'WHITE',
  Pink: 'LUMINOUS_VIVID_PINK'
};

var searchOptions = {
  shouldSort: true,
  tokenize: true,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    'species',
    'num'
  ]
};
const fuse = new Fuse(Object.values(pokedex), searchOptions);

// Set the display attributes for the command
dex.description = 'Fetch a lightweight pokédex entry';
dex.usage = 'ldex <name>';

// Set the command's action
dex.execute = async ({ msg, args }) => {
  // Do not execute the command without any arguments
  if (!args) return;

  // Declare empty flags and create string to manipulate out of args
  const flags = {};
  let toParse = args;

  // Create base embed to return
  const baseEmbed = new Discord.RichEmbed();

  // Detect if the Pokémon is shiny
  if (toParse.indexOf('shiny ') > -1) {
    toParse = toParse.replace('shiny ', '');
    flags.shiny = true;
  }

  // Detect if the Pokémon is a mega evolution
  if (toParse.indexOf('mega ') > -1) {
    toParse = toParse.replace('mega ', '');
    flags.mega = true;
  }

  // Detect if the Pokémon is Alolan
  if (toParse.indexOf('alolan ') > -1) {
    toParse = toParse.replace('alolan ', '');
    flags.alolan = true;
  }

  const reg = toParse.match(/(gmax|gigantamax|gigamax)/)
  if (reg) {
    toParse = toParse.replace(reg[0], '');
    flags.gmax = true;
  }

  let search = toParse;

  if (flags.mega) {
    search += ' mega';
  }

  if (flags.alolan) {
    search += ' alola';
  }

  if (flags.gmax) {
    search += ' gmax'
  }

  const results = fuse.search(search);
  const pokemon = results[0];
  const pokeName = Object.keys(pokedex)[Object.values(pokedex).indexOf(pokemon)];

  // If the pokémon is not found, return an error
  if (!pokemon) {
    msg.channel.send('Pokémon not found.');
    return;
  }

  // Initialize the embed with the title, image and colour
  baseEmbed.setTitle(pokemon.species)
    .setColor(colours[pokemon.color]);

  // Push appropriate fields to the embed
  baseEmbed.addField(`Type${pokemon.types.length > 1 ? 's' : ''}`, pokemon.types.join(', '), true)
    .addField('Abilities', Object.values(pokemon.abilities).join(', '), true)
    .addField('Base Stats', Object.keys(statTags).map(t => `${statTags[t]}: **${pokemon.baseStats[t]}**`).join(', '))
    .addField('External Resources', externalString(pokemon.species));

  // Set the footer for the Pokémon
  baseEmbed.setFooter(
    `#${pokemon.num}`,
    `https://play.pokemonshowdown.com/sprites/gen5/${pokeName}.png`
  );

  msg.channel.send({ embed: baseEmbed });
  logger.info(`Sent Pokémon ${pokemon.species} (light dex) to guild ${msg.guild.name}`);
};

// Function to generate the "External Resources" part of the embed
const externalString = name => {
  // Empty array to store link strings
  const externals = [];

  // Generate links for each site
  externals.push(
    `[Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/${name.replace(
      / /g,
      '_'
    )}_(Pokémon\\))`
  );
  externals.push(
    `[Smogon](https://www.smogon.com/dex/ss/pokemon/${name
      .toLowerCase()
      .replace(/ /g, '_')})`
  );
  externals.push(
    `[PokémonDB](https://pokemondb.net/pokedex/${name
      .toLowerCase()
      .replace(/ /g, '-')})`
  );
  externals.push(
    `[Serebii](https://www.serebii.net/pokedex-swsh/${name
      .toLowerCase()
      .replace(/ /g, '')}/)`
  );

  // Return concatenated list
  return externals.join(' • ');
};

module.exports = dex;
