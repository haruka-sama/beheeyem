/*
 * Beheeyem
 */

const { ShardingManager } = require('discord.js');
const { join } = require('path');
const config = require('../config');
const logger = require('./core/logger');

// Initialize the bot
const manager = new ShardingManager(join(__dirname, './core/discord-client.js'), { token: config.client.token });
manager.spawn();

manager.on('launch', (shard) => { logger.info(`Launched Shard #${shard.id}`) });

const guildCounts = [];
manager.on('message', (shard, msg) => {
  if (msg.type === 'guildCount') {
    guildCounts.push(msg.count);
    if (guildCounts.length === manager.totalShards) {
      logger.info('Beheeyem is ready!');
      const count = guildCounts.reduce((a, b) => a + b, 0);
      logger.info(`Running on ${manager.totalShards} shards in a total of ${count} guilds.`);
      manager.shards.forEach(i => {
        i.eval(`this.user.setPresence({ game: { name: "${count} guilds", type: "WATCHING" } })`);
      });
    }
  }
});

require('./core/settings');
