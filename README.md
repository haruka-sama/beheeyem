# Beheeyem

Beheeyem is a Discord bot that aims to provide information on the Pokémon games in an easy manner.

## Installing Dependencies

Install dependencies using your preferred package manager.  
`npm i` or `yarn`

## Running

`npm start` or `yarn start`

* When running for the **first time**, run `cache.sh` to load external databases before starting the bot.